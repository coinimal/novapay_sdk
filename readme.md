# NovapaySDK

## Api Calls

```
$app->get('/api/v1/scopes',['uses' => 'MerchantController@getScopes']);
$app->post('/api/v1/crypto/invoice', ['middleware' => 'oauth:merchant_nova_pay', 'uses' => 'MerchantController@createBTCInvoice']);
$app->get('/api/v1/crypto/invoice', ['middleware' => 'oauth:merchant_nova_pay', 'uses' => 'MerchantController@listBTCInvoice']);
$app->get('/api/v1/invoice/{invoice_id}', [ 'uses' => 'MerchantController@getInvoice']);
$app->post('/api/v1/quickbuy/invoice', ['middleware' => 'oauth:merchant_quickbuy', 'uses' => 'MerchantController@createQuickbuyInvoice']);
$app->get('/api/v1/quickbuy/invoice', ['middleware' => 'oauth:merchant_quickbuy', 'uses' => 'MerchantController@listQuickbuyInvoice']);
$app->post('/api/v1/subscription', ['middleware' => 'oauth:merchant_subscription', 'uses' => 'MerchantController@createSubscription']);
$app->get('/api/v1/subscription', ['middleware' => 'oauth:merchant_subscription', 'uses' => 'MerchantController@listSubscriptions']);
$app->get('/api/v1/subscription/invoice', ['middleware' => 'oauth:merchant_subscription', 'uses' => 'MerchantController@listSubscriptionInvoice']);
```

### TODO

 * config als array
 
 
#### Create Invoices

Als composer package machen, mit guzzle als dependency

3 arten von invocies
Classen: BitcoinInv. QuickbuyInv. SubscriptionInvoice ( Array Access )
 
+ eigene Classe Subscription, ( Array Access, Iterator )

```
foreach subscription as invoice {}
```


#### Filter

```
        $availabe_filters = [
            'fiat_amount', 'fiat_amount_lte', 'fiat_amount_lt', 'fiat_amount_gt','fiat_amount_gte', 'meta', 'status', 'user_email', 'id','runtime','active'
        ];
```


return als array von invoices

#### Exceptions

für Serveroffline usw, Validation