<?php
namespace NovaPay;
require_once('NovaPayConfig.php');
require_once('ConnectionHandler.php');

abstract class Validate
{
	protected $data;
	protected $_id;
	protected $_accessToken;

	protected $_conf;

	// these are kind of abstract properties:	
	protected $_type;
	protected $_url;
	protected $_getUrl;
	protected $_numerics;
	protected $_urls;
	protected $_email;
	protected $_alnum;
	protected $_ignore;

	protected $_id_is_unique = true;

	protected function _isAlNum($value)
	{
		$orig = strlen($value);
		$value = preg_replace('/[^0-9a-zA-Z]/', '', $value);
		$new = strlen($value);
		return $orig==$new;
	}
	protected function _isId($value)
	{
		$orig = strlen($value);
		$value = preg_replace('/[^-0-9a-zA-Z]/', '', $value);
		$new = strlen($value);
		return $orig==$new;
	}

	public function __set_helper($name, $value)
	{
		if(in_array($name, $this->_numerics) and is_numeric($value)) return true;
		elseif(in_array($name, $this->_urls) and filter_var($value, FILTER_VALIDATE_URL)) return true;
		elseif(in_array($name, $this->_email) and filter_var($value, FILTER_VALIDATE_EMAIL)) return true;
		elseif(in_array($name, $this->_alnum) and $this->_isAlNum($value)) return true;
		elseif(in_array($name, $this->_ignore)) return true;
		elseif(!$this->_id_is_unique and $name=='id' and $this->isId($value)) return true;
		else return false;
	}
	public function __set($name, $value)
	{
		if(is_array($value))
		{
			foreach($value as $val)
			{
				if($this->__set_helper($name,$val)) $this->data[$name][] = $val;
			}
		}
		elseif($this->__set_helper($name,$value)) $this->data[$name] = $value;
		elseif($name=='id' and $this->_isId($value))
		{
			$this->_id = $value;
			$this->data = array();
		}
		elseif($name=='access_token' and $this->_isAlNum($value))
		{
			$this->_accessToken = $value;
			$this->data['access_token'] = $value;
		}
		//else throw new DomainException('wrong');//
		else echo 'Failed: '.$name.PHP_EOL;
	}

	public function __get($name)
	{
		if(!empty($this->data) && array_key_exists($name, $this->data))
		{
			return $this->data[$name];
		}
		if($name=='id' and $this->_id!=NULL)
		{
			return $this->_id;
		}
	}

	public function __construct(array $arguments=null)
	{
		global $NovaPayConfig;
		$this->_conf = $NovaPayConfig;
		$this->_accessToken = $NovaPayConfig['access_token'];
		if(is_array($arguments))
		{
			foreach($arguments as $key=>$val)
			{
				$this->$key = $val;
			}
		}
		return $this;
	}
}

abstract class BaseInvoice extends Validate
{
	public function __construct(array $arguments=null)
	{
		$this->_numerics = ['amount'];
		$this->_urls = ['redirect_url', 'redirect_url_failure'];
		$this->_email = ['user_email'];
		$this->_alnum = ['currency'];
		$this->_ignore = [];
		parent::__construct($arguments);
	}

	public function create()
	{
		$conn = new ConnectionHandler();
		if(!isset($this->data['access_token'])) $this->data['access_token'] = $this->_accessToken;
		if(!isset($this->data['currency'])) $this->data['currency'] = $this->_conf['currency'];
		$data = $conn->post($this->_url, $this->data);
		$this->_id = $data['id'];
		$this->data = $data['attributes'];
		return true;
	}
	public function get()
	{
		$conn = new ConnectionHandler();
		$data = $conn->get($this->_getUrl, $this->_id, ['access_token'=>$this->_accessToken]); $this->data = $data['attributes'];
		return true;
	}
	public function __copy_from_array(array $array)
	{
		$this->_id = $array['id'];
		$this->data = $array['attributes'];
	}
}

abstract class BaseSearch extends Validate
{
	public $invoices = array();

	protected $_invoiceClass;

	public function __construct(array $arguments=null)
	{
		$this->_numerics = ['active', 'fiat_amount', 'fiat_amount_lte', 'fiat_amount_lt', 'fiat_amount_gt', 'fiat_amount_gte', 'runtime'];
		$this->_urls = [];
		$this->_email = ['user_email'];
		$this->_alnum = [];
		$this->_ignore = ['meta', 'status'];
		$this->_id_is_unique = false;
		parent::__construct($arguments);
	}

	public function search() {
		$this->get();
	}
	public function get()
	{
		$conn = new ConnectionHandler();
		if(!isset($this->data['access_token'])) $this->data['access_token'] = $this->_accessToken;
		$data = $conn->get_json($this->_url, $this->data);
		$this->_set_invoices($data);
		return true;
	}
	private function _set_invoices($invoices)
	{
		foreach( $invoices as $key=>$val )
		{
			$tmp = new $this->_invoiceClass();
			$tmp->__copy_from_array($val);
			$this->invoices[] = $tmp;
		}
	}
}
class InvoiceSearch extends BaseSearch
{
	public function __construct(array $arguments=null)
	{
		parent::__construct($arguments);
		$this->_invoiceClass = Invoice::class;
		$this->_url = '/api/v1/crypto/invoice';
	}
}
class QuickbuyInvoiceSearch extends BaseSearch
{
	public function __construct(array $arguments=null)
	{
		parent::__construct($arguments);
		$this->_invoiceClass = QuickbuyInvoice::class;
		$this->_url = '/api/v1/quickbuy/invoice';
	}
}
class SubscriptionInvoiceSearch extends BaseSearch
{
	public function __construct(array $arguments=null)
	{
		parent::__construct($arguments);
		$this->_invoiceClass = SubscriptionInvoice::class;
		$this->_url = '/api/v1/subscription';
	}
}

class Subscription extends Validate
{
	public $invoices = array();

	public function __construct(array $arguments=null)
	{
		$this->_numerics = ['amount','runtime'];
		$this->_urls = ['redirect_url', 'redirect_url_failure'];
		$this->_email = ['user_email'];
		$this->_alnum = ['currency','interval'];
		$this->_ignore = [];
		$this->_type = 'subscription';
		$this->_url = '/api/v1/subscription';
		$this->_getUrl = '/api/v1/';
		parent::__construct($arguments);
	}

	public function create()
	{
		$conn = new ConnectionHandler();
		if(!isset($this->data['currency'])) $this->data['currency'] = $this->_conf['currency'];
		if(!isset($this->data['access_token'])) $this->data['access_token'] = $this->_accessToken;
		$data = $conn->post($this->_url, $this->data);
		$this->_id = $data['id'];
		$invoices = $data['attributes']['invoices'];
		unset($data['attributes']['invoices']);
		$this->_set_invoices($invoices);
		$this->data = $data['attributes'];
		return true;
	}
	public function get()
	{
		$conn = new ConnectionHandler();
		$data = $conn->get($this->_getUrl, $this->_id, ['access_token'=>$this->_accessToken]);
		$invoices = $data['attributes']['invoices'];
		unset($data['attributes']['invoices']);
		$this->_set_invoices($invoices);
		$this->data = $data['attributes'];
		return true;
	}
	private function _set_invoices($invoices)
	{
		foreach( $invoices as $key=>$val )
		{
			$tmp = new SubscriptionInvoice();
			$tmp->__copy_from_array($val);
			$this->invoices[] = $tmp;
		}
	}
}
class Invoice extends BaseInvoice
{
	public function __construct(array $arguments=null)
	{
		parent::__construct($arguments);
		$this->_type = 'btc_invoice';
		$this->_url = '/api/v1/crypto/invoice';
		$this->_getUrl = '/api/v1/invoice';
	}
}
class QuickbuyInvoice extends BaseInvoice
{
	public function __construct(array $arguments=null)
	{
		parent::__construct($arguments);
		$this->_type = 'quickbuy_invoice';
		$this->_url = '/api/v1/quickbuy/invoice';
		$this->_getUrl = '/api/v1/invoice';
	}
}
class SubscriptionInvoice extends BaseInvoice
{
	public function __construct(array $arguments=null)
	{
		parent::__construct($arguments);
		$this->_type = 'subscription_invoice';
		$this->_url = '/api/v1/subscription/invoice';
		$this->_getUrl = '/api/v1/subscription/invoice';
	}
	public function create()
	{
		// throw some error, because this makes no sense	
	}
}
// usage examples:
$arguments = array(
	'amount' => 100,
	'redirect_url' => 'http://success.org',
	'redirect_url_failure' => 'https://www.notsucces.net',
	'user_email'=>'alpha@beta.com',
	'currency' => 'EUR',
	'noooooo' => 'dsf'
);

$inv = new Invoice($arguments);
$inv->amount = "alles"; // throws error
$inv->amount = 150; // works
$inv->blubb = 3; // error
$inv->create(); 
echo $inv->id .' '. $inv->crypto_amount, PHP_EOL;

$inv->get();
echo $inv->id .' '. $inv->fiat_amount, PHP_EOL;
try {
$inv2 = new Invoice();
$inv2->id = 'blabla';
$inv2->get();
echo $inv2->id .' '. $inv2->fiat_amount, PHP_EOL;
} catch(\Exception $exception) {
	echo $exception->getMessage(), PHP_EOL;
}
$sub = new Subscription($arguments);
$sub->runtime = 100;
$sub->interval = 'P1M';
$sub->create();
foreach( $sub->invoices as $key => $val )
{
	echo $val->crypto_amount, PHP_EOL;
}

$search = new QuickbuyInvoiceSearch();
$search->fiat_amount_gt = 100;
$search->search();
echo $search->invoices[0]->crypto_amount, PHP_EOL;

