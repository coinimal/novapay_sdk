<?php
namespace NovaPay;

require_once dirname(__FILE__)."/../vendor/autoload.php";
use GuzzleHttp\Client;

class NovapayServerException extends \Exception
{
	protected $message = 'Server went bananas!';
	protected $code = 500;
}
class NovapayClientException extends \Exception
{
	protected $message = 'Server went to the Bahamas!';
	protected $code = 400;
}
class NovapayConnectException extends \Exception
{
	protected $message = 'Could not connect!';
	protected $code = 100;
}

class ConnectionHandler
{
	public $host = 'http://api.novapay.io';
	public $error = '';
	public $httpStatus = 0;
	public $response = '';

	private $_client;
	private $_debug = false; // set the debug flag in guzzle queries

	public function __construct()
	{
		$this->_client = new Client(['base_uri' => $this->host]);
	}

	private function _handle_exceptions($exception)
	{
		if($exception instanceof GuzzleHttp\Exception\ServerException)
		{
			throw new NovapayServerException();
		}
		else if($exception instanceof GuzzleHttp\Exception\ClientException)
		{
			throw new NovapayClientException();
		}
		else if($exception instanceof GuzzleHttp\Exception\ConnectException)
		{
			throw new NovapayConnectException();
		}
		else
		{
			//echo get_class($exception), PHP_EOL;
			echo $exception->getMessage().PHP_EOL; 
			echo 'I don\'t like this'.PHP_EOL;
		}
	}
	public function post($url, $data)
	{
		try {
			$response = $this->_client->post($url,['debug'=>$this->_debug, 'form_params'=>$data]);
			if($response->getStatusCode()==201) {
				return json_decode($response->getBody()->getContents(),true)['data'];
			}
		}
		catch (\Exception $exception)
		{
			$this->_handle_exceptions($exception);
		}
	}
	public function get($url, $id, $data)
	{
		try {
			$response = $this->_client->get($url.'/'.$id,['debug'=>$this->_debug, 'query'=>$data]);
			if($response->getStatusCode()==200) {
				return json_decode($response->getBody()->getContents(),true)['data'];
			}
		}
		catch (\Exception $exception)
		{
			$this->_handle_exceptions($exception);
		}
	}
	public function get_json($url, $data)
	{
		try {
			$send['access_token'] = $data['access_token'];
			unset($data['access_token']);
			if(!empty($data)) $send['filter'] = json_encode($data);
			$response = $this->_client->get($url,['debug'=>$this->_debug, 'query'=>$send]);
			if($response->getStatusCode()==200) {
				return json_decode($response->getBody()->getContents(),true)['data'];
			}
		}
		catch (\Exception $exception)
		{
			$this->_handle_exceptions($exception);
		}
	}
}

